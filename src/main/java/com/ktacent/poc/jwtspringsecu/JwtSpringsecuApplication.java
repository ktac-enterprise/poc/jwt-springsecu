package com.ktacent.poc.jwtspringsecu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtSpringsecuApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtSpringsecuApplication.class, args);
	}

}
