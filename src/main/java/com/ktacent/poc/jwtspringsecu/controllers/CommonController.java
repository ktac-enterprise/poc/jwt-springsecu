/**
 * 
 */
package com.ktacent.poc.jwtspringsecu.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author alexk
 *
 */
@RestController
public class CommonController {
	
	@RequestMapping({"/hello"})
	public String firstPage() {
		return "Hello World";
	}

}
